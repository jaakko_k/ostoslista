$(document).ready(function() {
	
	var savedList = {
		"product": "",
		"quantity": "",
		"price": ""
	};
	
	var itemArray = [];
	
	var lastInArr;
		
	$("#save_item").click(function() {

		savedList.product = $("#p_name").val();
		savedList.quantity = $("#p_quantity").val();
		savedList.price = $("#p_price").val();
		
		itemArray.push(savedList);	
		
		lastInArr = $(itemArray).last()[0];
		
		$("#saved_list").append('<li title="Edit item"><span class="prod_span">' + lastInArr.product + 
								'</span><span class="quant_span">' + lastInArr.quantity + 
								'</span><span class="price_span">' + lastInArr.price + 
								'</span><input class="del_item" type="button" value="Delete item"></input></li>');
		
		$("li").each(function() {
			var that = this;
			$("input", this).click(function() {
				that.remove();
				if (!$("li").length) {
					$("#delete_list").remove();
				}
			});
			$(".prod_span", this).click(function() {
				var first = $(this, that).text();
				var second = $(".quant_span", that).text();
				var third = $(".price_span", that).text();
				$("#p_name").val(first);
				$("#p_quantity").val(second);
				$("#p_price").val(third);
				that.remove();
				if (!$("li").length) {
					$("#delete_list").remove();
					itemArray = [];
				}
			});
		});
	
		if (!$("#delete_list").length) {
			$("#delete_list_btn").append('<input id="delete_list" type="button" value="Delete list"></input>');
		}
		
		$("#delete_list").click(function() {
			$("ul").empty();
			$(this).remove();
			itemArray = [];
		});
	
	});
		
});